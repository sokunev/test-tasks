Solution contains tests to check for each task if it is implemented correctly.
Test names are prepended with task number. Implement the given task with as much tests passing as you can.

Task1
Distinct by Id,
Source collection can have the same object present several times. Implement a method that produces 
a collection without objects with repeating Id.
Changes should be limited to DistinctByIdService, method signature is not to be changed.

Task2
Predefined order
Source collection is unordered. Implement a method to output source collection in specified order.
Changes should be limited to OrderingService, method signature is not to be changed

Task3
Tree structure
Source is a colleciton of items with Id and ParentId fields, where ParentId points at parent item. 
Implement a method that transforms this plain structure into tree structure where parent entity 
has a list of child entities.
Along with changes to TreeCreationService, it is allowed to use inheritance to extend entities, if needed,
but method signature in the service is not to be changed.

Task4
Create pairs
Source is two arrays of similar objects. Object from first array can have a pair in second array - object that has equal id.
Task is to create array containing all pairs.

