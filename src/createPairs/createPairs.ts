import { SingleEntity, PairEntity } from "./entities.type";

export function createPairs(left: SingleEntity[], right: SingleEntity[]): PairEntity[] {
  const rightToMap = new Map<number, SingleEntity>();
  right.forEach(rightEntity => rightToMap.set(rightEntity.id, rightEntity));

  const res = [];
  left.forEach(leftEntity => {
    if (rightToMap.has(leftEntity.id)) {
      const obj = {
        "left": leftEntity,
        "right": rightToMap.get(leftEntity.id)
      };
      res.push(obj);
    }
  });

  return res;
}
