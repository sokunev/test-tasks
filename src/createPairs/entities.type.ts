export interface SingleEntity {
  id: number;
  text: string;
}

export interface PairEntity {
  left: SingleEntity;
  right: SingleEntity;
}
