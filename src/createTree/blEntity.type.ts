export interface BlEntity {
  id: number;
  children: BlEntity[];
  text: string;
}
