import { DalEntity } from "./dalEntity.type";
import { BlEntity } from "./blEntity.type";

export function createTree(dal: DalEntity[]): BlEntity[] {
  const tree = [];
  const dalToBlMap = new Map<number, BlEntity>();

  dal.forEach(dalEntity => {
    const blEntity: BlEntity = {
      id: dalEntity.id,
      text: dalEntity.text,
      children: [],
    };

    dalToBlMap.set(blEntity.id, blEntity);
  });

  dal.forEach(dalEntity => {
    const currentDalEntity = dalToBlMap.get(dalEntity.id);

    if (dalEntity.parentId !== null) {
      const parent = dalToBlMap.get(dalEntity.parentId);
      parent.children.push(currentDalEntity);
    }

    if (dalEntity.parentId === null) {
      tree.push(currentDalEntity);
    }
  });

  return tree;
}

