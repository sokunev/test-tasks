import { DistinctEntity } from "./entity.type";

/// <summary>
/// returns only objects with unique ids, dropping objects,
///  which ids are already in the output sequence
/// </summary>
/// <param name="entities">input collection</param>
/// <returns>distinct output collection</returns>
export function distinctById(entities: DistinctEntity[]): DistinctEntity[] {
  const entitiesToMap = new Map<number, DistinctEntity>();

  for (let idx = 0; idx < entities.length; idx++) {
    const elemId = entities[idx].id;

    if (!entitiesToMap.has(elemId)) {
      entitiesToMap.set(elemId, entities[idx]);
    }
  }

  return Array.from(entitiesToMap.values());
}

