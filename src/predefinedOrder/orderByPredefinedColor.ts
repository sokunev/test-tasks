import { ColoredEntity } from "./entity.type";
import { Color } from "./color.type";

/// <summary>
/// returns items from input collection in order, specified by "predefined" collection
/// </summary>
/// <param name="entities">input collection</param>
/// <param name="order">defines order</param>
/// <returns></returns>
export function orderByPredefinedColor(entities: ColoredEntity[], order: Color[]): ColoredEntity[] {
  return [];
}
