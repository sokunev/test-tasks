import { expect } from "chai";
import { createPairs } from "../src/createPairs/createPairs";
import { PairEntity, SingleEntity } from "../src/createPairs/entities.type";
import { testConstants } from "./testConstants";
import { measure } from "./measure";

describe("createPairs", function (): void {
  it("should return all pairs", function (): void {
    // arrange
    const left: SingleEntity[] = [{ id: 1, text: "a" }, { id: 3, text: "b" }, { id: 4, text: "z" }];
    const right: SingleEntity[] = [{ id: 2, text: "c" }, { id: 1, text: "d" }, { id: 4, text: "y" }];

    const expected = [{
      "left": { "id": 1, "text": "a" },
      "right": { "id": 1, "text": "d" }
    },
    {
      "left": { "id": 4, "text": "z" },
      "right": { "id": 4, "text": "y" }
    }
    ];

    // act
    const result = createPairs(left, right);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should pass performance constraint", function (): void {
      // arrange
      const input: SingleEntity[] = [];
      for (let i = 0; i < testConstants.targetSize; i++) {
        input.push({
          id: i,
          text: "some text"
        });
      }

      // act
      let result: PairEntity[];
      const time = measure(() => result = createPairs(input, input));
      // assert
      expect(time < testConstants.maxTime);
      expect(result.length).equals(input.length);
    });

});
