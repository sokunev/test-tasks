import { expect } from "chai";
import { DistinctEntity } from "../src/distinctById/entity.type";
import { distinctById } from "../src/distinctById/distinctById";
import { testConstants } from "./testConstants";
import { measure } from "./measure";

describe("distinctById", function (): void {
  it("should return only unique entities", function (): void {
    // arrange
    const input: DistinctEntity[] = [
      {
        id: 1,
        name: "a"
      },
      {
        id: 4,
        name: "b"
      },
      {
        id: 1,
        name: "ab"
      },
      {
        id: 4,
        name: "b"
      },
      {
        id: 1,
        name: "ab"
      }
    ];

    const expected = [
      {
        id: 1,
        name: "a"
      },
      {
        id: 4,
        name: "b"
      },
    ];

    // act
    const result = distinctById(input);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should pass performance constraint", function (): void {
    // arrange
    const input: DistinctEntity[] = [];
    for (let i = 0; i < testConstants.targetSize; i++) {
      input.push({
        id: i,
        name: "name"
      });
    }

    // act
    let result: DistinctEntity[];
    const time = measure(() => result = distinctById(input));
    // assert
    expect(time < testConstants.maxTime);
    expect(result.length).equals(input.length);
  });
});
