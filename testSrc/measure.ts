
// returns time in seconds that action has taken
export function measure(action: () => void): number {
  const start = new Date().getTime();
  action();
  const end = new Date().getTime();
  let res = (end - start);
  console.log(`Method performd by (msec):  ${res}`);
  return res ; // end - start
}
