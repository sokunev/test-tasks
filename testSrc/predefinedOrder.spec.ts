import { expect } from "chai";
import { Color, enumValues} from "../src/predefinedOrder/color.type";
import { ColoredEntity } from "../src/predefinedOrder/entity.type";
import { orderByPredefinedColor } from "../src/predefinedOrder/orderByPredefinedColor";
import { testConstants } from "./testConstants";
import { measure } from "./measure";
import * as _ from "lodash";

describe("predefinedOrder", function (): void {
  it("should return  sorted array", function (): void {
    // arrange
    const input: ColoredEntity[] = [
      {
        id: 8,
        color: Color.Orange
      },
      {
        id: 4,
        color: Color.Black
      },
      {
        id: 0,
        color: Color.Yellow
      },
      {
        id: 1,
        color: Color.Black
      },
      {
        id: 1,
        color: Color.Green
      },
      {
        id: 1,
        color: Color.Pink
      }
    ];

    const orderColor: Color[] = [
      Color.Black,
      Color.Green,
      Color.Red,
      Color.Yellow
    ];

    const expected  = [
      {
        id: 4,
        color: Color.Black
      },
      {
        id: 1,
        color: Color.Black
      },
      {
        id: 1,
        color: Color.Green
      },
      {
        id: 0,
        color: Color.Yellow
      },
      {
        id: 8,
        color: Color.Orange
      },
      {
        id: 1,
        color: Color.Pink
      }
    ];

    // act
    const result = orderByPredefinedColor(input, orderColor);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should pass performance constraint", function (): void {
    // arrange
    const input: ColoredEntity[] = [];
    for (let i = 0; i < testConstants.targetSize; i++) {
      input.push({
        id: i,
        color: _.sample(enumValues)
      });
    }
    const orderColor: Color[] = [
      Color.Black,
      Color.Green,
      Color.Yellow,
      Color.Brown
    ];
    // act
    let result: ColoredEntity[];
    const time = measure(() => result = orderByPredefinedColor(input, orderColor));
    // assert
    expect(time < testConstants.maxTime);
    expect(result.length).equals(input.length);
  });
});
